<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Doctor extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'doctor';



    public static function showHospitalByDoctorId($doctor_id){


       return  DB::table('doctors_hospital')
            ->join('hospital', 'hospital.id', '=', 'doctors_hospital.hospital_id')
            ->select('hospital.id as hospital_id','hospital.name','hospital.city', 'hospital.street','hospital.zip','hospital.country','hospital.phone_mobile','hospital.phone_home','hospital.phone_fax','hospital.email')
            ->where('doctors_hospital.doctor_id', '=', $doctor_id)
            ->get();


    }


    public static function getDoctorTimeTableByHospitalId($hospital_id){

        return  DB::table('doctors_hospital_time_table')
            ->join('hospital', 'hospital.id', '=', 'doctors_hospital_time_table.hospital_id')
            ->select('date_time','hospital_id','hospital.name','hospital.city')
            ->where('hospital_id', '=', $hospital_id)
            ->get();

    }



}
