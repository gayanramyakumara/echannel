<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Specialty extends Model
{

    protected $table = 'specialty';

    public static function showSpecialtyByDoctorId($doctor_id){

        return  DB::table('doctors_specialty')
            ->join('specialty', 'specialty.id', '=', 'doctors_specialty.specialty_id')
            ->select('doctors_specialty.specialty_id as specialty_id','specialty.specialty')
            ->where('doctors_specialty.doctor_id', '=', $doctor_id)
            ->get();

    }



}
