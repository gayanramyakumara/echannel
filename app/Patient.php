<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Patient extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'patient';


    public  static  function  findPatientByTelephone($telephone){

        return  DB::table('patient')
            ->select('id','phone_home','phone_mobile','title','first_name','last_name','nic','address','email','is_active')
            ->where('is_active', '=', '1')
            ->where('phone_mobile', 'like', '%'.$telephone.'%')
            ->orWhere('phone_home', 'like', '%'.$telephone.'%')
            ->first();

    }
}
