<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Doctor;
use App\Hospital;
use App\Specialty;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;



class DoctorController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

        $doctors =  Doctor::all();

        return view('doctor.listDoctors', ['doctors' => $doctors]);
    }


    /**
     * create a new doctor.
     *
     * @return Response
     */

    public function create(Request $request)
    {


        $hospitals =  Hospital::all();
        $doctor_create = $request->input('doctor_create');

        if ($doctor_create == 1) {

            $doctor_working_hospitals = $request->input('doctor_working_hospitals');

            $doctor_create = array();

            $doctor_create['title'] = $request->input('doctor_title');
            $doctor_create['first_name'] = $request->input('doctor_first_name');
            $doctor_create['last_name'] = $request->input('doctor_last_name');
            $doctor_create['nic'] = $request->input('doctor_nic');
            $doctor_create['address'] = $request->input('doctor_address');
            $doctor_create['country'] = $request->input('doctor_country');
            $doctor_create['specialty'] = $request->input('doctor_speciality');
            $doctor_create['sub_specialty'] = $request->input('doctor_sub_speciality');
            $doctor_create['email'] = $request->input('doctor_email');
            $doctor_create['date_of_birth'] = $request->input('doctor_date_of_birth');
            $doctor_create['phone_mobile'] =  preg_replace('/[^0-9]+/', '', $request->input('doctor_mobile'));
            $doctor_create['phone_home'] =  preg_replace('/[^0-9]+/', '', $request->input('doctor_home'));
            $doctor_create['password'] = md5($request->input('doctor_password'));
            $doctor_create['pin_delivery_method'] = $request->input('doctor_pin_delivery_method');
            $doctor_create['is_active'] = 1;

            DB::table('doctor')->insert($doctor_create);
            $is_inserted_details =  DB::getPdo()->lastInsertId();

            if($is_inserted_details){

                $doctor_hospital_arr = array();

                foreach($doctor_working_hospitals as $doctor_hospital_val){

                    $doctor_hospital_arr['hospital_id']= $doctor_hospital_val;
                    $doctor_hospital_arr['doctor_id']= $is_inserted_details;

                    $is_inserted_doctors_hospital = DB::table('doctors_hospital')->insert($doctor_hospital_arr);

                }

                if($is_inserted_doctors_hospital){

                    $request->session()->flash('alert-success', 'Doctor added successfully');
                    return redirect()->action('DoctorController@index');

                }else{

                    return view('doctor.addDoctor',['hospitals'=>$hospitals]);

                }

            }

        } else {
            return view('doctor.addDoctor',['hospitals'=>$hospitals]);
        }

    }



    public function viewDoctorById($id){


        $doctor_result = Doctor::find($id);

        $doctor_hospital_result = Doctor::showHospitalByDoctorId($id);

       // $doctor_time_table_arr[] = array();


        foreach($doctor_hospital_result as $doctor_hospital_value){

            $hospital_id = $doctor_hospital_value->hospital_id;

            $timetable_result = Doctor::getDoctorTimeTableByHospitalId($hospital_id);


            $timetable_result = Doctor::getDoctorTimeTableByHospitalId($hospital_id);

            $doctor_time_table_arr[$doctor_hospital_value->name] = $timetable_result;


        }



        return view('doctor.viewDoctorById',[
                    'doctor' => $doctor_result,
                    'doctor_hospital_result'=>$doctor_hospital_result,
                    'doctor_time_table_arr'=>$doctor_time_table_arr
                ]);


    }



    public function getDoctorsHospitals(Request $request){


        $doctor_id =  $request->input('channel_doctor_id');

       return  $doctor_hospital_result = Doctor::showHospitalByDoctorId($doctor_id);
       // echo  json_encode($doctor_hospital_result);

    }


    public function getDoctorsSpecialtyById(Request $request){


        $doctor_id =  $request->input('doctor_id');

        return  $doctor_specialty_result = Specialty::showSpecialtyByDoctorId($doctor_id);
        // echo  json_encode($doctor_hospital_result);

    }



    public function createDoctorTimeDate(Request $request,$id=false){

        $doctor_hospital_result = Doctor::showHospitalByDoctorId($id);
        $doctor_available_hospital_create = $request->input('doctor_available_hospital_create');

        if ($doctor_available_hospital_create == 1) {

            $doctor_available_hospital = array();
            $doctor_id =  $request->input('doctor_id');

            $date_create = date_create($request->input('doctor_available_time'));

            $doctor_available_hospital['doctor_id'] = $doctor_id ;
            $doctor_available_hospital['hospital_id'] = $request->input('doctor_available_hospital');
            $doctor_available_hospital['date_time'] = date_format($date_create, 'Y-m-d H:i:s');
            $doctor_available_hospital['available'] = 1;

            if(DB::table('doctors_hospital_time_table')->insert($doctor_available_hospital)){

                $request->session()->flash('alert-success', 'Doctor\'s date and time added successfully');
                return redirect()->action('DoctorController@viewDoctorById',[$doctor_id]);

            }else{
                return view('doctor.addDoctorTimeAndDate',['doctor_hospital_result'=>$doctor_hospital_result,'doctor_id'=>$doctor_id]);
            }

        }else{

            return view('doctor.addDoctorTimeAndDate',['doctor_hospital_result'=>$doctor_hospital_result,'doctor_id'=>$id]);
        }

    }



}