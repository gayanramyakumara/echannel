<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Hospital;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
class HospitalController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

        $hospitals = Hospital::all();

        return view('hospital.listHospitals',['hospitals'=>$hospitals]);

    }





    /**
     * create a new hospital.
     *
     * @return Response
     */

    public function create(Request $request){



        
        $hospital_create    =   $request->input('hospital_create');

        if($hospital_create == 1){

            $hospital_data = array();

            $hospital_data['name'] = $request->input('hospital_name');
            $hospital_data['street'] = $request->input('hospital_street');
            $hospital_data['city'] = $request->input('hospital_city');
            $hospital_data['zip'] = $request->input('hospital_zip');
            $hospital_data['country'] = $request->input('hospital_country');
            $hospital_data['email'] = $request->input('hospital_email');
            $hospital_data['latitude'] = 7.196914; //$request->input('latitude');
            $hospital_data['longitude'] = 79.891205;// $request->input('longitude');
            $hospital_data['phone_home'] =  preg_replace('/[^0-9]+/', '', $request->input('hospital_phone_office'));
            $hospital_data['phone_mobile'] =  preg_replace('/[^0-9]+/', '', $request->input('hospital_mobile'));
            $hospital_data['phone_fax'] =  preg_replace('/[^0-9]+/', '', $request->input('hospital_phone_fax'));
            $hospital_data['is_active'] = 1;

             //dd($request->all());
            // dd($hospital_data);

            if(DB::table('hospital')->insert($hospital_data)){
                $request->session()->flash('alert-success', 'Hospital added successfully');
                return redirect()->action('HospitalController@index');
            }else{
                return view('hospital.addHospital');
            }

        }else{
            return view('hospital.addHospital');
        }



    }




    public function viewHospitalById($id){


        $hospital_result = Hospital::find($id);

        return view('hospital.viewHospitalById',['hospital' => $hospital_result]);


    }




}
