<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Patient;
use App\Doctor;
use App\Http\Controllers\Controller;
use DB;

class PatientController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

        $patients = Patient::all();

        return view('patient.listPatient',['patients'=>$patients]);

    }




    /**
     * create a new Patient.
     *
     * @return Response
     */

    public function create(Request $request){

        $patient_create    =   $request->input('patient_create');

        if($patient_create == 1){

            $patient_data = array();


            $patient_data['title'] = $request->input('patient_title');
            $patient_data['first_name'] = $request->input('patient_first_name');
            $patient_data['last_name'] = $request->input('patient_last_name');
            $patient_data['nic'] = $request->input('patient_nic');
            $patient_data['address'] = $request->input('patient_address');
            $patient_data['country'] = $request->input('patient_country');
            $patient_data['email'] = $request->input('patient_email');
            $patient_data['date_of_birth'] = $request->input('patient_date_of_birth');
            $patient_data['phone_mobile'] =  preg_replace('/[^0-9]+/', '', $request->input('patient_mobile'));
            $patient_data['phone_home'] =  preg_replace('/[^0-9]+/', '', $request->input('patient_home'));
            $patient_data['password'] = md5($request->input('patient_password'));
            $patient_data['pin_delivery_method'] = $request->input('patient_pin_delivery_method');
            $patient_data['is_active'] = 1;



            if(DB::table('patient')->insert($patient_data)){
                $request->session()->flash('alert-success', 'Patient added successfully');
                return redirect()->action('PatientController@index');
            }else{
                return view('patient.addPatient');
            }

        }else{
            return view('patient.addPatient');
        }

    }


    public function find_patient_by_telephone(Request $request){

         $telephone_num =  $request->input('telephone_id');
         $doctor_specialty_result = Patient::findPatientByTelephone($telephone_num);

        if($doctor_specialty_result){

            return  array(
                'id'=>$doctor_specialty_result->id,
                'title'=>$doctor_specialty_result->title,
                'first_name'=>$doctor_specialty_result->first_name,
                'last_name'=>$doctor_specialty_result->last_name,
                'address'=>$doctor_specialty_result->address ,
                'nic'=>$doctor_specialty_result->nic ,
                'email'=>$doctor_specialty_result->email,
                'phone_home'=>$doctor_specialty_result->phone_home,
                'phone_mobile'=>$doctor_specialty_result->phone_mobile
            );

        }else{
            return "0";
        }

    }



}
