<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Channel;
use App\Doctor;
use App\Patient;
use App\Specialty;
use App\Http\Controllers\Controller;
use DB;
class ChannelController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

      //  $hospitals = Channel::all();

    //    return view('hospital.listHospitals',['hospitals'=>$hospitals]);

    }




    /**
     * create a new hospital.
     *
     * @return Response
     */

    public function create(Request $request)
    {

        $doctors =  Doctor::all();
        $patient =  Patient::all();

        $channel_create = $request->input('channel_create');

        if ($channel_create == 1) {

            $patient_channel_create = array();
            $guest_user_details = array();

            $is_member  = $request->input('member_patient_id');

            $patient_channel_create['doctor_id']    = $request->input('channel_doctor_name');
            $patient_channel_create['hospital_id']  = $request->input('channel_doctors_hospital');
            $patient_channel_create['specialty_id'] = $request->input('channel_doctors_specialty');

            if($is_member){

                $patient_channel_create['patient_id'] = $is_member;


            }else{

                $guest_user_details['title'] = $request->input('guest_patient_title');
                $guest_user_details['first_name'] = $request->input('guest_patient_first_name');
                $guest_user_details['last_name'] = $request->input('guest_patient_last_name');
                $guest_user_details['email'] = $request->input('guest_patient_email');
                $guest_user_details['telephone'] = preg_replace('/[^0-9]+/', '',$request->input('guest_patient_telephone'));
                $guest_user_details['national_id_no'] = $request->input('guest_patient_nic');
                $guest_user_details['area'] = $request->input('guest_patient_area');
                $guest_user_details['nationality'] = $request->input('guest_patient_nationality');
                $guest_user_details['doctor_notification'] = $request->input('guest_patient_doctor_notification');

                $guest_user_details['member'] = "No";

                DB::table('guest_patient')->insert($guest_user_details);
                $is_inserted_details =  DB::getPdo()->lastInsertId();

                if($is_inserted_details){

                    $patient_channel_create['guest_user_id'] = $is_inserted_details;

                }

            }

            $patient_channel_create['payment_status'] = "1";
            $patient_channel_create['date_time'] =  date('Y-m-d H:i:s');

            DB::table('doctor_channel')->insert($patient_channel_create);


            return view('channel.addNewChannel',['doctors'=>$doctors,'patient'=>$patient]);


        } else {
            return view('channel.addNewChannel',['doctors'=>$doctors,'patient'=>$patient]);

        }

        //channel_doctor_name
        //channel_doctors_hospital
        //channel_doctors_specialty
        //member_patient_id
        //


    }




    }
