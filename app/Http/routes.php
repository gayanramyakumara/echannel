<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/




//get('/', function () {
//    return redirect('/patient');
//});

//Patient

get('patient', 'PatientController@index');
get('patient/create', 'PatientController@create');
post('patient/create', 'PatientController@create');
post('patient/find_patient_by_telephone', 'PatientController@find_patient_by_telephone');


//Doctor

get('doctor', 'DoctorController@index');
get('doctor/create', 'DoctorController@create');
post('doctor/create', 'DoctorController@create');
get('doctor/view/{id}', 'DoctorController@viewDoctorById');
post('doctor/find_hospitals', 'DoctorController@getDoctorsHospitals');
post('doctor/find_doctors_specialty', 'DoctorController@getDoctorsSpecialtyById');

get('doctor/create_doctor_time_table/{id}', 'DoctorController@createDoctorTimeDate');
post('doctor/create_doctor_time_table', 'DoctorController@createDoctorTimeDate');

//Hospitals

get('hospital', 'HospitalController@index');
get('hospital/create', 'HospitalController@create');
post('hospital/create', 'HospitalController@create');
get('hospital/view/{id}', 'HospitalController@viewHospitalById');


//Channel

get('channel', 'ChannelController@index');
get('channel/create', 'ChannelController@create');
post('channel/create', 'ChannelController@create');
