@extends('layouts.master')

@section('title', 'List Patients')

@section('content')


    <div class="wrap">

        <div class="head">
            <div class="info">
                <h1>Patients  </h1>
                <ul class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li><a href="#">Patient</a></li>
                    <li class="active">Show Patient</li>
                </ul>
            </div>

            <div class="search">
                <form method="post" action="">
                    <input type="text" placeholder="search..." class="form-control">
                    <button type="submit"><span class="i-calendar"></span></button>
                    <button type="submit"><span class="i-magnifier"></span></button>
                </form>
            </div>
        </div>

        <div class="container">

            <div class="row">

                <div class="col-md-12">


                    @if (session('alert-success'))
                        <div class="alert alert-success">
                            <strong>Well done ! </strong>  {{ session('alert-success') }}
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                        </div>

                    @endif


                    <div class="block">



                        <div class="head">
                            <h2>Show All Patients  </h2>
                            <div class="side fr">

                            </div>
                        </div>

                        <div class="content np">

                            <div class="content np table-sorting">

                                <table cellpadding="0" cellspacing="0" width="100%" class="simple_sort">
                                    <thead>
                                    <tr>

                                        <th width="25%">ID</th>
                                        <th width="25%">Name</th>
                                        <th width="25%">nic</th>
                                        <th width="25%">address</th>
                                        <th width="25%">Phone</th>
                                        <th width="25%">email</th>
                                        <th width="25%">pin_delivery_method</th>
                                        <th width="25%">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($patients as $patient_value)

                                       <tr>
                                           <td><input type="checkbox" name="checkbox"/></td>
                                           <td>{{$patient_value->title." ".$patient_value->first_name." ".$patient_value->last_name}}</td>
                                           <td>{{$patient_value->nic}}</td>
                                           <td>{{$patient_value->address. " ".$patient_value->country}}</td>
                                           <td>  {{$patient_value->phone_mobile ." / ".$patient_value->phone_home}}</td>

                                           <td>{{$patient_value->email}}</td>
                                           <td>{{$patient_value->pin_delivery_method}}</td>
                                           <td>
                                               <a href="#">
                                                   <span class="i-pencil text-danger"></span>
                                               </a>
                                               <a href="#">
                                                   <span class=" i-file text-success"></span>
                                               </a>
                                               <a href="#">
                                                   <span class="i-trashcan text-danger"></span>
                                               </a>

                                           </td>
                                       </tr>

                                    @endforeach


                                    </tbody>
                                </table>

                            </div>

                        </div>

                </div>

            </div>






        </div>

    </div>




@stop