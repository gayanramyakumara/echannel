@extends('layouts.master')

@section('title', 'Add Patient')

@section('content')


    <div class="wrap">

        <div class="head">
            <div class="info">
                <h1>Patients  </h1>
                <ul class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li><a href="#">Patient</a></li>
                    <li class="active">Create Patient</li>
                </ul>
            </div>

            <div class="search">
                <form method="post" action="">
                    <input type="text" placeholder="search..." class="form-control">
                    <button type="submit"><span class="i-calendar"></span></button>
                    <button type="submit"><span class="i-magnifier"></span></button>
                </form>
            </div>
        </div>

        <div class="container">

            <div class="row">

                <div class="col-md-12">

                    <div class="block">
                        <div class="head">
                            <h2>Create a new patient  </h2>
                            <div class="side fr">

                            </div>
                        </div>
                        <div class="content np">

                            {!!Form::open(array('action' => 'PatientController@create','id' => 'validate', 'class' => 'create_new_patient','role'=>'form','onclick'=>'javascript'))!!}

                            <div class="controls-row">
                                <div class="col-md-3">
                                    {!! Form::label('patient_title', ' Title:', array('for' => 'patient_title'))!!}
                                </div>
                                <div class="col-md-9">
                                    {!! Form::select('patient_title',array('' => 'All','mr' => 'Mr', 'mrs' => 'Mrs', 'miss' => 'Miss', 'rev' => 'Rev',  'dr' => 'Dr','mest' => 'Mest', 'baby' => 'Baby'), Input::get('patient_title'), ['class' => 'validate[required] form-control','id'=>'patient_title']) !!}
                                </div>
                            </div>
                            <div class="controls-row">
                                <div class="col-md-3">
                                    {!! Form::label('patient_first_name', ' First Name:', array('for' => 'patient_first_name'))!!}
                                </div>
                                <div class="col-md-9">
                                    {!! Form::text('patient_first_name',Input::get("patient_first_name"), array('class' => 'validate[required] form-control', 'id'=>'patient_first_name','placeholder'=>''))!!}
                                </div>
                            </div>

                            <div class="controls-row">
                                <div class="col-md-3">
                                    {!! Form::label('patient_last_name', ' Last Name:', array('for' => 'patient_last_name'))!!}
                                </div>
                                <div class="col-md-9">
                                    {!! Form::text('patient_last_name',Input::get("patient_last_name"), array('class' => 'validate[required] form-control', 'id'=>'patient_last_name','placeholder'=>''))!!}
                                </div>
                            </div>


                            <div class="controls-row">
                                <div class="col-md-3">
                                    {!! Form::label('patient_nic', '  NIC:', array('for' => 'patient_nic'))!!}
                                </div>
                                <div class="col-md-9">
                                    {!! Form::text('patient_nic',Input::get("patient_nic"), array('class' => 'validate[required,maxSize[10]] form-control', 'id'=>'patient_nic','placeholder'=>''))!!}
                                </div>
                            </div>



                            <div class="controls-row">
                                <div class="col-md-3">
                                    {!! Form::label('patient_address', ' Address:', array('for' => 'patient_address'))!!}
                                </div>
                                <div class="col-md-9">
                                    {!! Form::text('patient_address',Input::get("patient_address"), array('class' => 'validate[required] form-control', 'id'=>'patient_address','placeholder'=>''))!!}
                                </div>
                            </div>


                            <div class="controls-row">
                                <div class="col-md-3">
                                    {!! Form::label('patient_country', ' Country:', array('for' => 'patient_country'))!!}
                                </div>
                                <div class="col-md-9">
                                    {!! Form::text('patient_country',Input::get("patient_country"), array('class' => 'validate[required] form-control', 'id'=>'patient_country','placeholder'=>''))!!}
                                </div>
                            </div>


                            <div class="controls-row">
                                <div class="col-md-3">
                                    {!! Form::label('patient_email', ' Email:', array('for' => 'patient_email'))!!}
                                </div>
                                <div class="col-md-9">
                                    {!! Form::text('patient_email',Input::get("patient_email"), array('class' => 'validate[required] form-control', 'id'=>'patient_email','placeholder'=>''))!!}
                                </div>
                            </div>

                            <div class="controls-row">
                                <div class="col-md-3">
                                    {!! Form::label('patient_date_of_birth', ' Date of Birth:', array('for' => 'patient_date_of_birth'))!!}
                                </div>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="i-calendar"></i></span>
                                        {!! Form::text('patient_date_of_birth',Input::get("patient_date_of_birth"), array('class' => 'validate[required] datepicker form-control', 'id'=>'patient_date_of_birth','placeholder'=>''))!!}
                                    </div>
                                </div>
                            </div>


                            <div class="controls-row">
                                <div class="col-md-3">
                                    {!! Form::label('patient_mobile', ' Telephone -  Mobile:', array('for' => 'patient_mobile'))!!}
                                </div>
                                <div class="col-md-9">
                                    {!! Form::text('patient_mobile',Input::get("patient_mobile"), array('class' => 'mask_phone validate[required] form-control', 'id'=>'patient_mobile','placeholder'=>''))!!}
                                    <span class="help-block">Example: (000) 000-00-00</span>
                                </div>
                            </div>



                            <div class="controls-row">
                                <div class="col-md-3">
                                    {!! Form::label('patient_home', ' Telephone - Home:', array('for' => 'patient_home'))!!}
                                </div>
                                <div class="col-md-9">
                                    {!! Form::text('patient_home',Input::get("patient_home"), array('class' => 'mask_phone validate[required] form-control', 'id'=>'patient_home','placeholder'=>''))!!}
                                    <span class="help-block">Example: (000) 000-00-00</span>
                                </div>
                            </div>



                            <div class="controls-row">
                                <div class="col-md-3">
                                    {!! Form::label('patient_password', ' Password:', array('for' => 'patient_password'))!!}
                                </div>
                                <div class="col-md-9">
                                    {!! Form::password('patient_password',Input::get("patient_password"), array('class' => 'validate[required] form-control', 'id'=>'patient_password','placeholder'=>''))!!}
                                </div>
                            </div>


                            <div class="controls-row">
                                <div class="col-md-3">
                                    {!! Form::label('patient_pin_delivery_method', ' PIN Delivery Method:', array('for' => 'patient_email'))!!}
                                </div>
                                <div class="col-md-9">
                                    {{--{!! Form::text('patient_pin_delivery_method',Input::get("patient_pin_delivery_method"), array('class' => 'validate[required] form-control', 'id'=>'patient_pin_delivery_method','placeholder'=>''))!!}--}}
                                    <label class="radio inline">

                                    {!!   Form::radio('patient_pin_delivery_method', 'sms', true) !!}
                                   SMS
                                    </label>

                                    <label class="radio inline">
                                        {!!   Form::radio('patient_pin_delivery_method', 'mail') !!}
                                        Mail
                                    </label>
                                </div>
                            </div>


                        </div>

                        <div class="footer">
                            <div class="side fr">
                                {!! Form::hidden('patient_create', '1') !!}
                                {!! Form::button('Create',array('type'=>'submit','class'=>'btn btn-primary','value'=>'patient_submit','name'=>'patient_submit')) !!}
                            </div>
                        </div>

                        {!!Form::close()!!}

                    </div>

                </div>

            </div>






        </div>

    </div>




@stop