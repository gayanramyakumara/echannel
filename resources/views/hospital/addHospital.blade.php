@extends('layouts.master')

@section('title', 'Add Hospital')

@section('content')


    <div class="wrap">

        <div class="head">
            <div class="info">
                <h1>Hospital  </h1>
                <ul class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li><a href="#">Hospital</a></li>
                    <li class="active">Create a hospital</li>
                </ul>
            </div>

            <div class="search">
                <form method="post" action="">
                    <input type="text" placeholder="search..." class="form-control">
                    <button type="submit"><span class="i-calendar"></span></button>
                    <button type="submit"><span class="i-magnifier"></span></button>
                </form>
            </div>
        </div>

        <div class="container">

            <div class="row">

                <div class="col-md-12">

                    <div class="block">
                        <div class="head">
                            <h2>Create a new hospital  </h2>
                            <div class="side fr">

                            </div>
                        </div>
                        <div class="content np">

                            {!!Form::open(array('action' => 'HospitalController@create','id' => 'validate', 'class' => 'create_new_hospital','role'=>'form','onclick'=>'javascript'))!!}


                            <div class="controls-row">
                                <div class="col-md-3">
                                    {!! Form::label('hospital_name', ' Hospital Name:', array('for' => 'hospital_name'))!!}
                                </div>
                                <div class="col-md-9">
                                    {!! Form::text('hospital_name',Input::get("hospital_name"), array('class' => 'validate[required] form-control', 'id'=>'hospital_name','placeholder'=>''))!!}
                                </div>
                            </div>





                            <div class="controls-row">
                                <div class="col-md-3">
                                    {!! Form::label('hospital_address', ' Address:', array('for' => 'hospital_address'))!!}
                                </div>

                                <div class="col-md-3 ">
                                    {!! Form::text('hospital_street',Input::get("hospital_street"), array('class' => 'validate[required]  form-control inline ', 'id'=>'hospital_city','placeholder'=>'Street'))!!}
                                </div>

                                <div class="col-md-3 ">
                                    {!! Form::text('hospital_city',Input::get("hospital_city"), array('class' => 'validate[required] form-control inline', 'id'=>'hospital_city','placeholder'=>'City'))!!}
                                </div>
                                <div class="col-md-3 ">
                                    {!! Form::text('hospital_zip',Input::get("hospital_zip"), array('class' => 'validate[required] form-control inline', 'id'=>'hospital_zip','placeholder'=>'Zip Code'))!!}
                                </div>

                            </div>


                            <div class="controls-row">
                                <div class="col-md-3">
                                    {!! Form::label('hospital_country', ' Country:', array('for' => 'hospital_country'))!!}
                                </div>
                                <div class="col-md-9">
                                    {!! Form::text('hospital_country',Input::get("hospital_country"), array('class' => 'validate[required] form-control', 'id'=>'hospital_country','placeholder'=>''))!!}
                                </div>
                            </div>


                            <div class="controls-row">
                                <div class="col-md-3">
                                    {!! Form::label('hospital_email', ' Email:', array('for' => 'hospital_email'))!!}
                                </div>
                                <div class="col-md-9">
                                    {!! Form::text('hospital_email',Input::get("hospital_email"), array('class' => 'validate[required] form-control', 'id'=>'hospital_email','placeholder'=>''))!!}
                                </div>
                            </div>




                            <div class="controls-row">
                                <div class="col-md-3">
                                    {!! Form::label('hospital_mobile', ' Telephone -  Mobile:', array('for' => 'hospital_mobile'))!!}
                                </div>
                                <div class="col-md-9">
                                    {!! Form::text('hospital_mobile',Input::get("hospital_mobile"), array('class' => 'mask_phone validate[required] form-control', 'id'=>'hospital_mobile','placeholder'=>''))!!}
                                    <span class="help-block">Example: (000) 000-00-00</span>
                                </div>
                            </div>



                            <div class="controls-row">
                                <div class="col-md-3">
                                    {!! Form::label('hospital_phone_office', ' Telephone - Office:', array('for' => 'hospital_phone_office'))!!}
                                </div>
                                <div class="col-md-9">
                                    {!! Form::text('hospital_phone_office',Input::get("hospital_phone_office"), array('class' => 'mask_phone validate[required] form-control', 'id'=>'hospital_phone_office','placeholder'=>''))!!}
                                    <span class="help-block">Example: (000) 000-00-00</span>
                                </div>
                            </div>


                            <div class="controls-row">
                                <div class="col-md-3">
                                    {!! Form::label('hospital_phone_fax', ' Telephone - Fax:', array('for' => 'hospital_phone_fax'))!!}
                                </div>
                                <div class="col-md-9">
                                    {!! Form::text('hospital_phone_fax',Input::get("hospital_phone_fax"), array('class' => 'mask_phone validate[required] form-control', 'id'=>'hospital_phone_fax','placeholder'=>''))!!}
                                    <span class="help-block">Example: (000) 000-00-00</span>
                                </div>
                            </div>





                        </div>

                        <div class="footer">
                            <div class="side fr">
                                {!! Form::hidden('hospital_create', '1') !!}
                                {!! Form::button('Create',array('type'=>'submit','class'=>'btn btn-primary','value'=>'hospital_submit','name'=>'hospital_submit')) !!}
                            </div>
                        </div>

                        {!!Form::close()!!}

                    </div>

                </div>

            </div>






        </div>

    </div>




@stop