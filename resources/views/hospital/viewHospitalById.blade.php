@extends('layouts.master')

@section('title', 'View Hospital Details')

@section('content')


    <div class="wrap">

        <div class="head">
            <div class="info">
                <h1>Hospital  </h1>
                <ul class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li><a href="#">Hospital</a></li>
                    <li class="active">View Hospital</li>
                </ul>
            </div>

            <div class="search">
                <form method="post" action="">
                    <input type="text" placeholder="search..." class="form-control">
                    <button type="submit"><span class="i-calendar"></span></button>
                    <button type="submit"><span class="i-magnifier"></span></button>
                </form>
            </div>
        </div>

        <div class="container">

            <div class="row">

                <div class="col-md-12">


                    @if (session('alert-success'))
                        <div class="alert alert-success">
                            <strong>Well done ! </strong>  {{ session('alert-success') }}
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                        </div>

                    @endif



                    <div class="row">

                        <div class="col-md-6">
                            <div class="block">
                                <div class="head">
                                    <h2>Hospital details</h2>
                                </div>
                                <div class="content np">
                                    <div class="controls-row">
                                        <div class="col-md-4">
                                            <img src="{{ URL::asset('assets/img/hospital_icon.png') }}" class="img-thumbnail" style="margin-bottom: 5px;">
                                        </div>

                                    </div>
                                    <div class="controls-row">
                                        <div class="col-md-4">Hospital Name:</div>
                                        <div class="col-md-8">
                                            <p class="doctor_details_p">{{ $hospital->name   }}</p>
                                        </div>
                                    </div>

                                    <div class="controls-row">
                                        <div class="col-md-4">E-Mail:</div>
                                        <div class="col-md-8">
                                            <p class=""> {{ $hospital->email  }}</p>
                                        </div>
                                    </div>

                                    <div class="controls-row">
                                        <div class="col-md-4">Address:</div>
                                        <div class="col-md-8">
                                            <p class="doctor_details_p">{{ $hospital->street." ".$hospital->city." ".$hospital->zip  }}</p>
                                        </div>
                                    </div>


                                    <div class="controls-row">
                                        <div class="col-md-4">Country:</div>
                                        <div class="col-md-8">
                                            <p class="doctor_details_p">  {{ $hospital->country  }} </p>
                                        </div>
                                    </div>



                                    <div class="controls-row">
                                        <div class="col-md-4">Phone - Mobile:</div>
                                        <div class="col-md-8">
                                            <p class="doctor_details_p">  {{ $hospital->phone_mobile  }} </p>
                                        </div>
                                    </div>


                                    <div class="controls-row">
                                        <div class="col-md-4">Phone - Office:</div>
                                        <div class="col-md-8">
                                            <p class="doctor_details_p">{{ $hospital->phone_home }} </p>
                                        </div>
                                    </div>

                                    <div class="controls-row">
                                        <div class="col-md-4">Phone - Fax:</div>
                                        <div class="col-md-8">
                                            <p class="doctor_details_p">{{ $hospital->phone_fax }} </p>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="block">
                                <div class="head">
                                    <h2>Branch Details</h2>
                                </div>
                                <div class="content np">

                                    <div class="controls-row">
                                        <div class="col-md-4">Specialty  :</div>
                                        <div class="col-md-8"> <p class="doctor_details_p"> </p> </div>
                                    </div>

                                    <div class="controls-row">
                                        <div class="col-md-4">Sub Specialty:</div>
                                        <div class="col-md-8"> <p class="doctor_details_p"> </p></div>
                                    </div>


                                </div>
                            </div>

                        </div>


                    </div>






                </div>






            </div>

        </div>



@stop