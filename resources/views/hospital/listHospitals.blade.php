@extends('layouts.master')

@section('title', 'List Doctors')

@section('content')


    <div class="wrap">

        <div class="head">
            <div class="info">
                <h1>Hospitals  </h1>
                <ul class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li><a href="#">Hospitals</a></li>
                    <li class="active">Show Hospitals</li>
                </ul>
            </div>

            <div class="search">
                <form method="post" action="">
                    <input type="text" placeholder="search..." class="form-control">
                    <button type="submit"><span class="i-calendar"></span></button>
                    <button type="submit"><span class="i-magnifier"></span></button>
                </form>
            </div>
        </div>

        <div class="container">

            <div class="row">

                <div class="col-md-12">


                    @if (session('alert-success'))
                        <div class="alert alert-success">
                            <strong>Well done ! </strong>  {{ session('alert-success') }}
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                        </div>

                    @endif


                    <div class="block">



                        <div class="head">
                            <h2>Show All Hospitals  </h2>
                            <div class="side fr">

                            </div>
                        </div>

                        <div class="content np">

                            <div class="content np table-sorting">

                                <table cellpadding="0" cellspacing="0" width="100%" class="simple_sort">
                                    <thead>
                                    <tr>

                                        <th width="25%">ID</th>
                                        <th width="25%">Name</th>
                                        <th width="25%">address</th>
                                        <th width="25%">Phone</th>
                                        <th width="25%">email</th>
                                        <th width="25%">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($hospitals as $hospital_value)

                                        <tr>
                                            <td><input type="checkbox" name="checkbox"/></td>
                                            <td>
                                                <a href="{{ url('hospital/view/'.$hospital_value->id) }}">
                                                    {{$hospital_value->name}}
                                                </a>
                                            </td>
                                            <td>   <p class="doctor_details_p"> {{$hospital_value->street. " ".$hospital_value->city ." ".$hospital_value->country}} </p></td>
                                            <td>  {{$hospital_value->phone_mobile ." / ".$hospital_value->phone_home}}</td>

                                            <td>{{$hospital_value->email}}</td>
                                            <td>
                                                <a href="{{ url('hospital/view/'.$hospital_value->id) }}">
                                                    <span class="i-pencil text-danger"></span>
                                                </a>
                                                <a href="#">
                                                    <span class=" i-file text-success"></span>
                                                </a>
                                                <a href="#">
                                                    <span class="i-trashcan text-danger"></span>
                                                </a>

                                            </td>
                                        </tr>

                                    @endforeach


                                    </tbody>
                                </table>






                            </div>

                        </div>

                    </div>

                </div>






            </div>

        </div>



@stop