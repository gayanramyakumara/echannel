@extends('layouts.master')

@section('title', 'Add New Channel')

@section('content')

    <?php
    //$doctors_data_array = array();
 //   foreach($doctors as $doctors_value){
  //      $doctors_data_array[$doctors_value->id] = $doctors_value->title." ".$doctors_value->first_name." ".$doctors_value->last_name;
  //  }
    ?>
    <div class="wrap">

        <div class="head">
            <div class="info">
                <h1>Patients  </h1>
                <ul class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li><a href="#">Channel</a></li>
                    <li class="active">Add a channel</li>
                </ul>
            </div>

            <div class="search">
                <form method="post" action="">
                    <input type="text" placeholder="search..." class="form-control">
                    <button type="submit"><span class="i-calendar"></span></button>
                    <button type="submit"><span class="i-magnifier"></span></button>
                </form>
            </div>
        </div>

        <div class="container">

            <div class="row">

                <div class="col-md-12">

                    <div class="block">
                        <div class="head">
                            <h2>Add a channel</h2>
                            <div class="side fr">

                            </div>
                        </div>
                        <div class="content np">

                            {!!Form::open(array('action' => 'ChannelController@create','id' => 'validate', 'class' => 'create_new_channel','role'=>'form','onclick'=>'javascript'))!!}

                            {!! Form::hidden('_token', csrf_token(),array('id'=>'_token')) !!}

                            <div class="controls-row">
                                <div class="col-md-3">
                                    {!! Form::label('channel_doctor_name', ' Doctor Name:', array('for' => 'channel_doctor_name'))!!}
                                </div>
                                <div class="col-md-9">

                                        <select name="channel_doctor_name" class="validate[required] form-control" id="channel_doctor_name" onchange="return findHospitalByDoctorId('{{csrf_token()}}');">
                                            <option value="">Select A Doctor</option>
                                            @foreach($doctors as $doctors_value)
                                                <option value="{{$doctors_value->id}}">{{$doctors_value->title." ".$doctors_value->first_name." ".$doctors_value->last_name}}</option>
                                            @endforeach
                                        </select>
{{--                                    {!! Form::select('channel_doctor_name',array($doctors_data_array), Input::get('channel_doctor_name'), ['class' => 'validate[required] form-control', 'onchange'=>'return findHospitalByDoctorId()','id'=>'channel_doctor_name']) !!}--}}
                                </div>

                            </div>

                            <div class="controls-row">
                                <div class="col-md-3">
                                    {!! Form::label('channel_doctors_hospital', ' Hospital :', array('for' => 'channel_doctors_hospital'))!!}
                                </div>
                                <div class="col-md-9" id="channel_doctors_hospital_div">
                                    <select name="channel_doctors_hospital" id="channel_doctors_hospital" class="validate[required] form-control channel_doctors_hospital_cls" disabled>
                                        {{--<option value="">All</option>--}}
                                    </select>
                                    {{--{!! Form::select('channel_doctors_hospital',array(''=>"All"), Input::get('channel_doctor_name'), ['class' => 'validate[required] form-control','id'=>'channel_doctors_hospital']) !!}--}}
                                </div>

                            </div>

                            <div class="controls-row">
                                <div class="col-md-3">
                                    {!! Form::label('channel_doctors_specialty', ' Specialty :', array('for' => 'channel_doctors_specialty'))!!}
                                </div>
                                <div class="col-md-9" id="channel_doctors_specialty_div">
                                    <select name="channel_doctors_specialty" id="channel_doctors_specialty" class="validate[required] form-control channel_doctors_specialty_cls">
                                        {{--<option value="">All</option>--}}
                                    </select>
                                    {{--{!! Form::select('channel_doctors_hospital',array(''=>"All"), Input::get('channel_doctor_name'), ['class' => 'validate[required] form-control','id'=>'channel_doctors_hospital']) !!}--}}
                                </div>

                            </div>


                            <div class="controls-row">
                                <div class="col-md-3">
                                    {!! Form::label('channel_patient_member', '  Member ? ', array('for' => 'channel_patient_member'))!!}
                                </div>

                                <div class="col-md-9">
                                    <div class="block">
                                        <div class="head">
                                            <h2>Are you a member in E-Channel ?</h2>
                                        </div>
                                        <div class="content tabs tabs-head ui-tabs ui-widget ui-widget-content ui-corner-all">

                                            <ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all" role="tablist">
                                                <li class="ui-state-default ui-corner-top ui-tabs-active ui-state-active" role="tab" tabindex="0" aria-controls="tabsh-1" aria-labelledby="ui-id-4" aria-selected="true"><a href="#tabsh-1" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-4"> Yes</a></li>
                                                <li class="ui-state-default ui-corner-top" role="tab" tabindex="-1" aria-controls="tabsh-2" aria-labelledby="ui-id-5" aria-selected="false"><a href="#tabsh-2" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-5"> No</a></li>
                                            </ul>

                                            <div id="tabsh-1" aria-labelledby="ui-id-4" class="ui-tabs-panel ui-widget-content ui-corner-bottom" role="tabpanel" aria-expanded="true" aria-hidden="false">

                                                <div class="controls-row">
                                                    <div class="col-md-3">
                                                    {!! Form::label('channel_patient_telephone', '  Enter Mobile number :', array('for' => 'channel_patient_telephone'))!!}
                                                    </div>


                                                <div class="col-md-7">
                                                    <div class="combobox ws">
                                                        <input type="text" class="form-control" id="patient_mobile_number" name="patient_mobile_number" value=""/>
                                                        <ul>
                                                            @foreach($patient as $patient_details)
                                                            <li value="{{ $patient_details->id }}">{{ $patient_details->phone_mobile }}</li>
                                                            <li value="{{ $patient_details->id }}">{{  $patient_details->phone_home }}</li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </div>

                                                <div class="col-md-2">
                                                    <input type="button" name="user_details_submit" class="btn btn-primary" value="Find !" id="" onclick="return findPatientDetailsByTelephoneNum('{{csrf_token()}}');"/>

                                                </div>

                                                <div class="controls searched_patient_details" id="searched_patient_details">
                                                    <div class="col-md-12">
                                                        <div class="block">
                                                            <div class="head">
                                                                <h2>Patient Details</h2>
                                                            </div>
                                                            <div class="content np">

                                                                <div class="controls-row">
                                                                    <div class="col-md-4">Full Name  :</div>
                                                                    <div class="col-md-8"> <p class="doctor_details_p" id="searched_patient_details_full_name"></p> </div>
                                                                </div>

                                                                <div class="controls-row">
                                                                    <div class="col-md-4">Email :</div>
                                                                    <div class="col-md-8"> <p class="doctor_details_p"  id="searched_patient_details_email"></p></div>
                                                                </div>

                                                                <div class="controls-row">
                                                                    <div class="col-md-4">NIC  :</div>
                                                                    <div class="col-md-8"> <p class="doctor_details_p"  id="searched_patient_details_nic"></p> </div>
                                                                </div>

                                                                <div class="controls-row">
                                                                    <div class="col-md-4">Address :</div>
                                                                    <div class="col-md-8"> <p class="doctor_details_p"  id="searched_patient_details_address"> </p></div>
                                                                </div>

                                                                <div class="controls-row">
                                                                    <div class="col-md-4">Phone- Home  :</div>
                                                                    <div class="col-md-8"> <p class="doctor_details_p"  id="searched_patient_details_phone_home"> </p> </div>
                                                                </div>

                                                                <div class="controls-row">
                                                                    <div class="col-md-4">Phone- Mobile :</div>
                                                                    <div class="col-md-8"> <p class="doctor_details_p"  id="searched_patient_details_phone_mobile"> </p></div>
                                                                </div>

                                                                <input type="hidden" name="member_patient_id" id="searched_patient_details_id"/>
                                                                {{--{!! Form::hidden('is_member', 'yes') !!}--}}
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>

                                                </div>

                                            </div>

                                            <div id="tabsh-2" aria-labelledby="ui-id-5" class="ui-tabs-panel ui-widget-content ui-corner-bottom" role="tabpanel" aria-expanded="false" aria-hidden="true" style="display: none;">

                                                <div class="controls-row">
                                                    <div class="col-md-3">
                                                    {!! Form::label('guest_patient_full_name', ' Full Name:', array('for' => 'guest_patient_full_name'))!!}
                                                    </div>
                                                    <div class="col-md-1">
                                                    {!! Form::select('guest_patient_title',array('' => 'All','mr' => 'Mr', 'mrs' => 'Mrs', 'miss' => 'Miss', 'rev' => 'Rev',  'dr' => 'Dr','mest' => 'Mest', 'baby' => 'Baby'), Input::get('guest_patient_title'), ['class' => 'validate[required] form-control','id'=>'guest_patient_title']) !!}
                                                    </div>
                                                    <div class="col-md-4">
                                                    {!! Form::text('guest_patient_first_name',Input::get("guest_patient_first_name"), array('class' => 'validate[required] form-control', 'id'=>'guest_patient_first_name','placeholder'=>''))!!}
                                                    </div>
                                                    <div class="col-md-4">
                                                    {!! Form::text('guest_patient_last_name',Input::get("guest_patient_last_name"), array('class' => 'validate[required] form-control', 'id'=>'guest_patient_last_name','placeholder'=>''))!!}
                                                    </div>
                                                </div>

                                                <div class="controls-row">
                                                    <div class="col-md-3">
                                                    {!! Form::label('guest_patient_nic', '  NIC:', array('for' => 'guest_patient_nic'))!!}
                                                    </div>
                                                    <div class="col-md-9">
                                                    {!! Form::text('guest_patient_nic',Input::get("patient_nic"), array('class' => 'validate[required,maxSize[10]] form-control', 'id'=>'guest_patient_nic','placeholder'=>''))!!}
                                                    </div>
                                                </div>


                                                <div class="controls-row">
                                                    <div class="col-md-3">
                                                    {!! Form::label('guest_patient_address', ' Address:', array('for' => 'guest_patient_address'))!!}
                                                    </div>
                                                    <div class="col-md-9">
                                                    {!! Form::text('guest_patient_address',Input::get("guest_patient_address"), array('class' => 'validate[required] form-control', 'id'=>'guest_patient_address','placeholder'=>''))!!}
                                                    </div>
                                                </div>


                                                <div class="controls-row">
                                                    <div class="col-md-3">
                                                    {!! Form::label('guest_patient_email', ' Email:', array('for' => 'guest_patient_email'))!!}
                                                    </div>
                                                    <div class="col-md-9">
                                                    {!! Form::text('guest_patient_email',Input::get("guest_patient_email"), array('class' => 'validate[required] form-control', 'id'=>'guest_patient_email','placeholder'=>''))!!}
                                                    </div>
                                                </div>


                                                <div class="controls-row">
                                                    <div class="col-md-3">
                                                    {!! Form::label('guest_patient_telephone', ' Telephone -  Mobile:', array('for' => 'guest_patient_telephone'))!!}
                                                    </div>
                                                    <div class="col-md-9">
                                                    {!! Form::text('guest_patient_telephone',Input::get("guest_patient_telephone"), array('class' => 'mask_phone validate[required] form-control', 'id'=>'guest_patient_telephone','placeholder'=>''))!!}
                                                    <span class="help-block">Example: (000) 000-00-00</span>
                                                    </div>
                                                </div>


                                                <div class="controls-row">
                                                    <div class="col-md-3">
                                                        {!! Form::label('guest_patient_area', ' Area :', array('for' => 'guest_patient_area'))!!}
                                                    </div>
                                                    <div class="col-md-9">
                                                        {!! Form::text('guest_patient_area',Input::get("guest_patient_area"), array('class' => 'validate[required] form-control', 'id'=>'guest_patient_area','placeholder'=>''))!!}
                                                    </div>
                                                </div>

                                                <div class="controls-row">
                                                    <div class="col-md-3">
                                                        {!! Form::label('guest_patient_nationality', ' Nationality :', array('for' => 'guest_patient_nationality'))!!}
                                                    </div>
                                                    <div class="col-md-9">
                                                        {!! Form::select('guest_patient_nationality',array('' => 'All','local' => 'Local', 'foreign' => 'Foreign'), Input::get('guest_patient_nationality'), ['class' => 'validate[required] form-control','id'=>'guest_patient_nationality']) !!}
                                                    </div>
                                                </div>

                                                <div class="controls-row">
                                                    <div class="col-md-3">
                                                        {!! Form::label('guest_patient_doctor_notification', ' Doctor Notification :', array('for' => 'guest_patient_doctor_notification'))!!}
                                                    </div>
                                                    <div class="col-md-9">
                                                        {!! Form::select('guest_patient_doctor_notification',array('' => 'All','yes' => 'Yes', 'no' => 'No'), Input::get('guest_patient_doctor_notification'), ['class' => 'validate[required] form-control','id'=>'guest_patient_doctor_notification']) !!}
                                                    </div>
                                                </div>

{{--                                                {!! Form::hidden('is_member', 'no') !!}--}}

                                            </div>

                                            </div>

                                        </div>
                                    </div>

                                </div>




                            <div class="controls-row">
                                <div class="col-md-3">

                                </div>
                                <div class="col-md-9">
                                    {!! Form::hidden('channel_create', '1') !!}
                                    {!! Form::button('Create',array('type'=>'submit','class'=>'btn btn-primary','value'=>'channel_submit','name'=>'channel_submit')) !!}
                                </div>
                            </div>



                        {!!Form::close()!!}

                    </div>




                </div>

            </div>


        </div>

    </div>


     <img src="{{ url('assets/img/loader.gif') }}" class="ajax_loader_position_abs"/>


    <script type="text/javascript">

//        $(document).ready(function(){
//            $("#searched_patient_details").hide();
//        });

        function findHospitalByDoctorId(token){

            $('.ajax_loader_position_abs').addClass('ajax_loader_position_abs_active');

            var channel_doctor_id = $('#channel_doctor_name').val();

                $.post('{{url('doctor/find_hospitals')}}', { channel_doctor_id : channel_doctor_id, '_token': token }, function(data) {
                    $('.ajax_loader_position_abs').removeClass('ajax_loader_position_abs_active');
                    if(data != ""){


                        $('.channel_doctors_hospital_cls').removeAttr('disabled', 'disabled');
                        $('.channel_doctors_hospital_cls').empty();
                        jQuery.each( data, function( i, val ) {
                            $('.channel_doctors_hospital_cls').append("<option value='"+val.hospital_id+"'>"+val.name+","+val.city+"</option>");
                        });
                        return false ;
                    }else{
                        $('.channel_doctors_hospital_cls').empty();

                        $('.channel_doctors_hospital_cls').attr('disabled', 'disabled');
                        return false ;
                    }

                    return false ;
                });


            $.post('{{url('doctor/find_doctors_specialty')}}', { doctor_id : channel_doctor_id, '_token': token }, function(data) {

                if(data != ""){

                    $('.channel_doctors_specialty_cls').empty();
                    jQuery.each( data, function( i, val ) {
                        $('.channel_doctors_specialty_cls').append("<option value='"+val.specialty_id+"'>"+val.specialty+"</option>");
                    });
                    return false ;

                }else{
                    $('.channel_doctors_specialty_cls').empty();
                    return false ;
                }


            });

        }


        function findPatientDetailsByTelephoneNum(token){

            $('.ajax_loader_position_abs').addClass('ajax_loader_position_abs_active');
            var telephone_id = $('#patient_mobile_number').val();

            $.post('{{url('patient/find_patient_by_telephone')}}', { telephone_id : telephone_id, '_token': token }, function(data) {

                if(data != 0){

                    $('#searched_patient_details').addClass('searched_patient_details_active');
                    $('.ajax_loader_position_abs').removeClass('ajax_loader_position_abs_active');

                     $("#searched_patient_details_full_name").html(data.title+" "+data.first_name+" "+data.last_name);
                     $("#searched_patient_details_email").html(data.email);
                     $("#searched_patient_details_nic").html(data.nic);
                     $("#searched_patient_details_phone_mobile").html(data.phone_mobile);
                     $("#searched_patient_details_phone_home").html(data.phone_home);
                     $("#searched_patient_details_address").html(data.address);
                     $("#searched_patient_details_id").val(data.id);

                }else{
                    $('.ajax_loader_position_abs').removeClass('ajax_loader_position_abs_active');
                    $('#searched_patient_details').removeClass('searched_patient_details_active');

                }




            });


            }

    </script>


@stop