@extends('layouts.master')

@section('title', 'Add Doctor')

@section('content')

    <?php
    $hospitals_data_array = array();
    foreach($hospitals as $hospitals_value){
        $hospitals_data_array[$hospitals_value->id] = $hospitals_value->name;
    }


    ?>

    <div class="wrap">

        <div class="head">
            <div class="info">
                <h1>Doctor  </h1>
                <ul class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li><a href="#">Doctor</a></li>
                    <li class="active">Create a doctor</li>
                </ul>
            </div>

            <div class="search">
                <form method="post" action="">
                    <input type="text" placeholder="search..." class="form-control">
                    <button type="submit"><span class="i-calendar"></span></button>
                    <button type="submit"><span class="i-magnifier"></span></button>
                </form>
            </div>
        </div>

        <div class="container">

            <div class="row">

                <div class="col-md-12">

                    <div class="block">
                        <div class="head">
                            <h2>Create a new doctor  </h2>
                            <div class="side fr">

                            </div>
                        </div>
                        <div class="content np">

                            {!!Form::open(array('action' => 'DoctorController@create','id' => 'validate', 'class' => 'create_new_doctor','role'=>'form','onclick'=>'javascript'))!!}

                            <div class="controls-row">
                                <div class="col-md-3">
                                    {!! Form::label('doctor_title', ' Title:', array('for' => 'doctor_title'))!!}
                                </div>
                                <div class="col-md-9">
                                    {!! Form::select('doctor_title',array('' => 'All','mr' => 'Mr', 'mrs' => 'Mrs', 'miss' => 'Miss', 'rev' => 'Rev',  'dr' => 'Dr','mest' => 'Mest', 'baby' => 'Baby'), Input::get('doctor_title'), ['class' => 'validate[required] form-control','id'=>'doctor_title']) !!}
                                </div>
                            </div>
                            <div class="controls-row">
                                <div class="col-md-3">
                                    {!! Form::label('doctor_first_name', ' First Name:', array('for' => 'doctor_first_name'))!!}
                                </div>
                                <div class="col-md-9">
                                    {!! Form::text('doctor_first_name',Input::get("doctor_first_name"), array('class' => 'validate[required] form-control', 'id'=>'doctor_first_name','placeholder'=>''))!!}
                                </div>
                            </div>

                            <div class="controls-row">
                                <div class="col-md-3">
                                    {!! Form::label('doctor_last_name', ' Last Name:', array('for' => 'doctor_last_name'))!!}
                                </div>
                                <div class="col-md-9">
                                    {!! Form::text('doctor_last_name',Input::get("doctor_last_name"), array('class' => 'validate[required] form-control', 'id'=>'doctor_last_name','placeholder'=>''))!!}
                                </div>
                            </div>


                            <div class="controls-row">
                                <div class="col-md-3">
                                    {!! Form::label('doctor_nic', '  NIC:', array('for' => 'doctor_nic'))!!}
                                </div>
                                <div class="col-md-9">
                                    {!! Form::text('doctor_nic',Input::get("doctor_nic"), array('class' => 'validate[required,maxSize[10]] form-control', 'id'=>'doctor_nic','placeholder'=>''))!!}
                                </div>
                            </div>



                            <div class="controls-row">
                                <div class="col-md-3">
                                    {!! Form::label('doctor_address', ' Address:', array('for' => 'doctor_address'))!!}
                                </div>
                                <div class="col-md-9">
                                    {!! Form::text('doctor_address',Input::get("doctor_address"), array('class' => 'validate[required] form-control', 'id'=>'doctor_address','placeholder'=>''))!!}
                                </div>
                            </div>


                            <div class="controls-row">
                                <div class="col-md-3">
                                    {!! Form::label('doctor_country', ' Country:', array('for' => 'doctor_country'))!!}
                                </div>
                                <div class="col-md-9">
                                    {!! Form::text('doctor_country',Input::get("doctor_country"), array('class' => 'validate[required] form-control', 'id'=>'doctor_country','placeholder'=>''))!!}
                                </div>
                            </div>


                            <div class="controls-row">
                                <div class="col-md-3">
                                    {!! Form::label('doctor_email', ' Email:', array('for' => 'doctor_email'))!!}
                                </div>
                                <div class="col-md-9">
                                    {!! Form::text('doctor_email',Input::get("doctor_email"), array('class' => 'validate[required] form-control', 'id'=>'doctor_email','placeholder'=>''))!!}
                                </div>
                            </div>

                            <div class="controls-row">
                                <div class="col-md-3">
                                    {!! Form::label('doctor_date_of_birth', ' Date of Birth:', array('for' => 'doctor_date_of_birth'))!!}
                                </div>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="i-calendar"></i></span>
                                        {!! Form::text('doctor_date_of_birth',Input::get("doctor_date_of_birth"), array('class' => 'validate[required] datepicker form-control', 'id'=>'doctor_date_of_birth','placeholder'=>''))!!}
                                    </div>
                                </div>
                            </div>


                            <div class="controls-row">
                                <div class="col-md-3">
                                    {!! Form::label('doctor_mobile', ' Telephone -  Mobile:', array('for' => 'doctor_mobile'))!!}
                                </div>
                                <div class="col-md-9">
                                    {!! Form::text('doctor_mobile',Input::get("doctor_mobile"), array('class' => 'mask_phone validate[required] form-control', 'id'=>'doctor_mobile','placeholder'=>''))!!}
                                    <span class="help-block">Example: (000) 000-00-00</span>
                                </div>
                            </div>



                            <div class="controls-row">
                                <div class="col-md-3">
                                    {!! Form::label('doctor_home', ' Telephone - Home:', array('for' => 'doctor_home'))!!}
                                </div>
                                <div class="col-md-9">
                                    {!! Form::text('doctor_home',Input::get("doctor_home"), array('class' => 'mask_phone validate[required] form-control', 'id'=>'doctor_home','placeholder'=>''))!!}
                                    <span class="help-block">Example: (000) 000-00-00</span>
                                </div>
                            </div>

                            <div class="controls-row">
                                <div class="col-md-3">
                                    {!! Form::label('doctor_working_hospitals', ' Working Hospitals :', array('for' => 'doctor_working_hospitals'))!!}
                                </div>

                                <div class="col-md-9">

                                    {!! Form::select('doctor_working_hospitals[]',array($hospitals_data_array), Input::get('doctor_working_hospitals'), ['class' => 'select2','multiple'=>'multiple','style'=>'width:200px','id'=>'doctor_working_hospitals']) !!}

                                    {{--<select name="doctor_working_hospitals[]" class="select2" style="width: 200px;"   multiple="multiple">--}}

                                            {{--<option value="1">Option 1</option>--}}
                                            {{--<option value="2">Option 2</option>--}}
                                            {{--<option value="3">Option 3</option>--}}
                                            {{--<option value="4" >Option 4</option>--}}
                                            {{--<option value="5">Option 5</option>--}}
                                            {{--<option value="6">Option 6</option>--}}

                                            {{--<option value="7">Option 7</option>--}}
                                            {{--<option value="8">Option 8</option>--}}
                                            {{--<option value="9">Option 9</option>--}}
                                            {{--<option value="10">Option 10</option>--}}

                                    {{--</select>--}}
                                </div>
                            </div>

                            <div class="controls-row">
                                <div class="col-md-3">
                                    {!! Form::label('doctor_password', ' Password:', array('for' => 'doctor_password'))!!}
                                </div>
                                <div class="col-md-9">
                                    {!! Form::password('doctor_password',Input::get("doctor_password"), array('class' => 'validate[required] form-control', 'id'=>'doctor_password','placeholder'=>''))!!}
                                </div>
                            </div>



                            <div class="controls-row">
                                <div class="col-md-3">
                                    {!! Form::label('doctor_speciality', ' Speciality:', array('for' => 'doctor_speciality'))!!}
                                </div>
                                <div class="col-md-9">
                                    {!! Form::select('doctor_speciality',array('' => 'All','anaesthetics' => 'Anaesthetics', 'genetics' => 'Genetics',), Input::get('doctor_speciality'), ['class' => 'validate[required] form-control','id'=>'doctor_speciality']) !!}
                                </div>
                            </div>

                            <div class="controls-row">
                                <div class="col-md-3">
                                    {!! Form::label('doctor_sub_speciality', 'Sub Speciality:', array('for' => 'doctor_sub_speciality'))!!}
                                </div>
                                <div class="col-md-9">
                                    {!! Form::select('doctor_sub_speciality',array('' => 'All','anaesthetics' => 'Anaesthetics', 'genetics' => 'Genetics',), Input::get('doctor_sub_speciality'), ['class' => 'validate[required] form-control','id'=>'doctor_sub_speciality']) !!}
                                </div>
                            </div>





                            <div class="controls-row">
                                <div class="col-md-3">
                                    {!! Form::label('doctor_pin_delivery_method', ' PIN Delivery Method:', array('for' => 'doctor_pin_delivery_method'))!!}
                                </div>
                                <div class="col-md-9">
                                    {{--{!! Form::text('patient_pin_delivery_method',Input::get("patient_pin_delivery_method"), array('class' => 'validate[required] form-control', 'id'=>'patient_pin_delivery_method','placeholder'=>''))!!}--}}
                                    <label class="radio inline">

                                        {!!   Form::radio('doctor_pin_delivery_method', 'sms', true) !!}
                                        SMS
                                    </label>

                                    <label class="radio inline">
                                        {!!   Form::radio('doctor_pin_delivery_method', 'mail') !!}
                                        Mail
                                    </label>
                                </div>
                            </div>


                        </div>

                        <div class="footer">
                            <div class="side fr">

                                {!! Form::hidden('doctor_create', '1') !!}
                                {!! Form::button('Create',array('type'=>'submit','class'=>'btn btn-primary','value'=>'doctor_submit','name'=>'doctor_submit')) !!}
                            </div>
                        </div>

                        {!!Form::close()!!}

                    </div>

                </div>

            </div>






        </div>

    </div>




@stop