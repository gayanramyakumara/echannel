@extends('layouts.master')

@section('title', 'Add Doctor')

@section('content')

    <?php
    $hospitals_data_array = array();
    foreach($doctor_hospital_result as $doctor_hospital_value){

          $hospitals_data_array[$doctor_hospital_value->hospital_id] = $doctor_hospital_value->name;

    }

    ?>

    <div class="wrap">

        <div class="head">
            <div class="info">
                <h1>Doctor  </h1>
                <ul class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li><a href="#">Doctor</a></li>
                    <li class="active">Create a doctor</li>
                </ul>
            </div>

            <div class="search">
                <form method="post" action="">
                    <input type="text" placeholder="search..." class="form-control">
                    <button type="submit"><span class="i-calendar"></span></button>
                    <button type="submit"><span class="i-magnifier"></span></button>
                </form>
            </div>
        </div>

        <div class="container">

            <div class="row">

                <div class="col-md-12">


                    @if (session('alert-success'))
                        <div class="alert alert-success">
                            <strong>Well done ! </strong>  {{ session('alert-success') }}
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                        </div>

                    @endif




                    <div class="block">
                        <div class="head">
                            <h2>Create a new doctor  </h2>
                            <div class="side fr">

                            </div>
                        </div>
                        <div class="content np">

                            {!!Form::open(array('action' => 'DoctorController@createDoctorTimeDate','id' => 'validate', 'class' => 'create_new_doctor','role'=>'form','onclick'=>'javascript'))!!}


                            <div class="controls-row">
                                <div class="col-md-3">
                                    {!! Form::label('doctor_available_hospital', ' Available Hospital :', array('for' => 'doctor_available_hospital'))!!}
                                </div>

                                <div class="col-md-9">

                                    {!! Form::select('doctor_available_hospital',$hospitals_data_array, Input::get('doctor_available_hospital'), ['class' => 'validate[required] form-control','id'=>'doctor_available_hospital']) !!}

                                </div>
                            </div>


                            <div class="controls-row">
                                <div class="col-md-3">
                                    {!! Form::label('doctor_available_time', ' Date and Time:', array('for' => 'doctor_available_time'))!!}
                                </div>
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="i-calendar-2"></i></span>
                                        <input type="text" class="datetimepicker form-control" name="doctor_available_time" />
                                    </div>
                                </div>
                            </div>




                        </div>

                        <div class="footer">
                            <div class="side fr">
                                {!! Form::hidden('doctor_available_hospital_create', '1') !!}
                                {!! Form::hidden('doctor_id', $doctor_id) !!}
                                {!! Form::button('Create',array('type'=>'submit','class'=>'btn btn-primary','value'=>'doctor_submit','name'=>'doctor_submit')) !!}
                            </div>
                        </div>

                        {!!Form::close()!!}

                    </div>

                </div>

            </div>






        </div>

    </div>




@stop