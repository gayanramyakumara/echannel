@extends('layouts.master')

@section('title', 'View Doctor Details')

@section('content')


    <div class="wrap">

        <div class="head">
            <div class="info">
                <h1>Doctors  </h1>
                <ul class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li><a href="#">Doctors</a></li>
                    <li class="active">View Doctors</li>
                </ul>
            </div>

            <div class="search">
                <form method="post" action="">
                    <input type="text" placeholder="search..." class="form-control">
                    <button type="submit"><span class="i-calendar"></span></button>
                    <button type="submit"><span class="i-magnifier"></span></button>
                </form>
            </div>
        </div>

        <div class="container">

            <div class="row">

                <div class="col-md-12">


                    @if (session('alert-success'))
                        <div class="alert alert-success">
                            <strong>Well done ! </strong>  {{ session('alert-success') }}
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                        </div>

                    @endif



                            <div class="row">

                                <div class="col-md-6">
                                    <div class="block">
                                        <div class="head">
                                            <h2>Doctor Personal details</h2>
                                        </div>
                                        <div class="content np">
                                            <div class="controls-row">
                                                <div class="col-md-4">
                                                    <img src="http://www.buzzinghealth.com/avatars/doctor_%20logo.jpg" class="img-thumbnail" style="margin-bottom: 5px;">
                                                </div>

                                            </div>
                                            <div class="controls-row">
                                                <div class="col-md-4">Full Name:</div>
                                                <div class="col-md-8">
                                                    <p class="doctor_details_p">{{ $doctor->title ." : ".$doctor->first_name ." ".$doctor->last_name  }}</p>
                                                </div>
                                            </div>

                                            <div class="controls-row">
                                                <div class="col-md-4">E-Mail:</div>
                                                <div class="col-md-8">
                                                    <p class=""> {{ $doctor->email  }}</p>
                                                </div>
                                            </div>

                                            <div class="controls-row">
                                                <div class="col-md-4">Address:</div>
                                                <div class="col-md-8">
                                                    <p class="doctor_details_p">{{ $doctor->address  }}</p>
                                                </div>
                                            </div>

                                            <div class="controls-row">
                                                <div class="col-md-4">NIC:</div>
                                                <div class="col-md-8">
                                                    <p class="doctor_details_p"> {{ $doctor->nic  }}</p>
                                                </div>
                                            </div>

                                            <div class="controls-row">
                                                <div class="col-md-4">Country:</div>
                                                <div class="col-md-8">
                                                    <p class="doctor_details_p">  {{ $doctor->country  }} </p>
                                                </div>
                                            </div>

                                            <div class="controls-row">
                                                <div class="col-md-4">Date of birth:</div>
                                                <div class="col-md-8">
                                                    <p class="doctor_details_p"> {{ $doctor->date_of_birth  }} </p>
                                                </div>
                                            </div>

                                            <div class="controls-row">
                                                <div class="col-md-4">Phone - Mobile:</div>
                                                <div class="col-md-8">
                                                    <p class="doctor_details_p">  {{ $doctor->phone_mobile  }} </p>
                                                </div>
                                            </div>


                                            <div class="controls-row">
                                                <div class="col-md-4">Phone - Home:</div>
                                                <div class="col-md-8">
                                                    <p class="doctor_details_p">{{ $doctor->phone_home }} </p>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="block">
                                        <div class="head">
                                            <h2>Professional Details</h2>
                                        </div>
                                        <div class="content np">

                                            <div class="controls-row">
                                                <div class="col-md-4">Specialty  :</div>
                                                <div class="col-md-8"> <p class="doctor_details_p">{{ $doctor->specialty }}</p> </div>
                                            </div>

                                            <div class="controls-row">
                                                <div class="col-md-4">Sub Specialty:</div>
                                                <div class="col-md-8"> <p class="doctor_details_p">{{ $doctor->sub_specialty }}</p></div>
                                            </div>


                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="block">
                                        <div class="head">
                                            <h2>Working Hospitals</h2>
                                        </div>
                                        <div class="content np">

                                            <div class="controls-row">
                                                @foreach($doctor_hospital_result as $doctor_hospital_values)
                                                    <div class="col-md-6">
                                                        <a href="{{url('hospital/view/'.$doctor_hospital_values->hospital_id )}}">{{ $doctor_hospital_values->name }} </a>
                                                    </div>
                                                @endforeach
                                            </div>




                                        </div>
                                    </div>

                                </div>



                                <div class="col-md-6">

                                    <div class="block">
                                        <div class="head">
                                            <h2>Hospital Timetable</h2>
                                            <div class="side fr">
                                                <a href="{{ url('doctor/create_doctor_time_table/'.$doctor->id) }}" class="btn btn-success">Create a time</a>
                                            </div>
                                        </div>
                                        <div class="content accordion">



                                            @foreach($doctor_time_table_arr as $key => $value)

                                                <h3>  {{ $key }} </h3>

                                                    <div class="np">
                                                        <table cellpadding="0" cellspacing="0" width="100%" class="list">
                                                            <tbody>
                                                                @foreach($value as $doctor_time_table_inner)
                                                                    <?php
                                                                    $datetime = explode(" ",$doctor_time_table_inner->date_time);
                                                                    ?>
                                                                    <tr>
                                                                        <td><p> {{ $datetime[0] }}</p></td>
                                                                        <td><p> {{ $datetime[1] }}  </p> </td>
                                                                        <td> <span class="label label-success">  </span></td>
                                                                    </tr>

                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>


                                            @endforeach

                                        </div>
                                    </div>



                                </div>






                            </div>






                </div>






            </div>

        </div>



@stop