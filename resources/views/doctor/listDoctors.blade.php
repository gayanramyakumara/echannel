@extends('layouts.master')

@section('title', 'List Doctors')

@section('content')


    <div class="wrap">

        <div class="head">
            <div class="info">
                <h1>Doctors  </h1>
                <ul class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li><a href="#">Doctors</a></li>
                    <li class="active">Show Doctors</li>
                </ul>
            </div>

            <div class="search">
                <form method="post" action="">
                    <input type="text" placeholder="search..." class="form-control">
                    <button type="submit"><span class="i-calendar"></span></button>
                    <button type="submit"><span class="i-magnifier"></span></button>
                </form>
            </div>
        </div>

        <div class="container">

            <div class="row">

                <div class="col-md-12">


                    @if (session('alert-success'))
                        <div class="alert alert-success">
                            <strong>Well done ! </strong>  {{ session('alert-success') }}
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                        </div>

                    @endif


                    <div class="block">



                        <div class="head">
                            <h2>Show All Doctors  </h2>
                            <div class="side fr">

                            </div>
                        </div>

                        <div class="content np">

                            <div class="content np table-sorting">

                                <table cellpadding="0" cellspacing="0" width="100%" class="simple_sort">
                                    <thead>
                                    <tr>

                                        <th width="25%">ID</th>
                                        <th width="25%">Name</th>
                                        <th width="25%">nic</th>
                                        <th width="25%">address</th>
                                        <th width="25%">Phone</th>
                                        <th width="25%">email</th>
                                        <th width="25%">specialty</th>
                                        <th width="25%">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($doctors as $doctor_value)

                                        <tr>
                                            <td><input type="checkbox" name="checkbox"/></td>
                                            <td>
                                                <a href="{{ url('doctor/view/'.$doctor_value->id) }}">
                                                    {{$doctor_value->title." ".$doctor_value->first_name." ".$doctor_value->last_name}}
                                                </a>
                                            </td>
                                            <td>{{$doctor_value->nic}}</td>
                                            <td>{{$doctor_value->address. " ".$doctor_value->country}}</td>
                                            <td>  {{$doctor_value->phone_mobile ." / ".$doctor_value->phone_home}}</td>

                                            <td>{{$doctor_value->email}}</td>
                                            <td>{{$doctor_value->specialty}}</td>
                                            <td>

                                                <a href="{{ url('doctor/create_doctor_time_table/'.$doctor_value->id) }}">
                                                    <span class="i-calendar text-default"></span>
                                                </a>

                                                <a href="{{ url('doctor/view/'.$doctor_value->id) }}">
                                                    <span class="i-pencil text-danger"></span>
                                                </a>
                                                <a href="#">
                                                    <span class=" i-file text-success"></span>
                                                </a>
                                                <a href="#">
                                                    <span class="i-trashcan text-danger"></span>
                                                </a>

                                            </td>
                                        </tr>

                                    @endforeach


                                    </tbody>
                                </table>






                            </div>

                        </div>

                    </div>

                </div>






            </div>

        </div>



@stop