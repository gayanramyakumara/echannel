<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patient', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('nic');
            $table->string('address');
            $table->string('country');
            $table->string('date_of_birth');
            $table->integer('phone_mobile');
            $table->integer('phone_home');
            $table->string('email')->unique();
            $table->string('password', 60);
            $table->enum('pin_delivery_method', ['mail', 'sms']);
            $table->integer('is_active');
            $table->rememberToken();
            $table->timestamps();
        });
    }




    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('patient');
    }
}
