<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDoctorsHospitalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('doctors_hospital', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('doctor_id')->unsigned();
            $table->integer('hospital_id')->unsigned();

            $table->foreign('hospital_id')->references('id')->on('hospital');
            $table->foreign('doctor_id')->references('id')->on('doctor');
            $table->timestamps();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('doctors_hospital');
    }
}
