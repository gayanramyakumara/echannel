<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDoctorsSpecialtyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctors_specialty', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('doctor_id')->unsigned();
            $table->integer('specialty_id')->unsigned();

            $table->foreign('specialty_id')->references('id')->on('specialty');
            $table->foreign('doctor_id')->references('id')->on('doctor');

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('doctors_specialty');
    }
}
