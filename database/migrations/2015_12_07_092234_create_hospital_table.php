<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHospitalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('hospital', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('city');
            $table->string('street');
            $table->string('zip');
            $table->string('country');
            $table->integer('phone_mobile');
            $table->integer('phone_office');
            $table->integer('phone_fax');
            $table->string('email')->unique();
            $table->string('latitude');
            $table->string('longitude');
            $table->integer('is_active');
            $table->rememberToken();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('hospital');
    }
}
