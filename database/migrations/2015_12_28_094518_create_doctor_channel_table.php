<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDoctorChannelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('doctor_channel', function (Blueprint $table) {

            $table->increments('id');
           // 2015_12_28_094518_create_doctor_channel_table  -  8
            $table->integer('doctor_id')->unsigned();
            $table->integer('hospital_id')->unsigned();
            $table->integer('guest_user_id')->nullable();
            $table->integer('specialty_id')->unsigned();
            $table->integer('patient_id')->nullable();

            $table->dateTime('date_time');
            $table->string('payment_status');

            $table->foreign('doctor_id')->references('id')->on('doctor');
            $table->foreign('hospital_id')->references('id')->on('hospital');
            $table->foreign('guest_user_id')->references('id')->on('guest_patient');
            $table->foreign('specialty_id')->references('id')->on('specialty');
            $table->foreign('patient_id')->references('id')->on('patient');

            $table->rememberToken();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('doctor_channel');
    }
}
