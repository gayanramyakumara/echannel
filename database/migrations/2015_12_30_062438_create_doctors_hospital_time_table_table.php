<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDoctorsHospitalTimeTableTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctors_hospital_time_table', function (Blueprint $table) {

            $table->increments('id');

            $table->integer('doctor_id')->unsigned();
            $table->integer('hospital_id')->unsigned();

            $table->dateTime('date_time');
            $table->string('available');

            $table->foreign('doctor_id')->references('id')->on('doctor');
            $table->foreign('hospital_id')->references('id')->on('hospital');

            $table->rememberToken();
            $table->timestamps();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('doctors_hospital_time_table');
    }
}
