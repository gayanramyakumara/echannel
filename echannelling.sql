-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 22, 2015 at 05:07 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `echannelling`
--

-- --------------------------------------------------------

--
-- Table structure for table `doctor`
--

CREATE TABLE IF NOT EXISTS `doctor` (
`id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nic` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_of_birth` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone_mobile` int(11) NOT NULL,
  `phone_home` int(11) NOT NULL,
  `specialty` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sub_specialty` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `pin_delivery_method` enum('mail','sms') COLLATE utf8_unicode_ci NOT NULL,
  `is_active` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `doctor`
--

INSERT INTO `doctor` (`id`, `title`, `first_name`, `last_name`, `nic`, `address`, `country`, `date_of_birth`, `phone_mobile`, `phone_home`, `specialty`, `sub_specialty`, `email`, `password`, `pin_delivery_method`, `is_active`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Dr', 'Gayan', 'Ramya Kumara', '883350120V', 'Kadawata Rd,Ragama', 'Sri Lanka', '12/07/2015', 771238845, 112954356, 'anaesthetics', 'genetics', 'gayan@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'mail', 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Dr', 'Kamala', 'Gunwaradana', '9537374464', 'Galle Rd,Dehiwala', 'Sri Lanka', '12/21/2015', 783246327, 372324783, 'genetics', 'genetics', 'kamala@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'mail', 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Dr', 'Nimal', 'Gunapala', '77665533V', 'Waskaduwa Rd,Wadduwa', 'Sri Lanka', '12/09/2015', 2147483647, 1234567654, 'anaesthetics', 'genetics', 'nimal@123.lk', 'b282b6a30a5a58feb422c68fc940f3f0', 'sms', 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `doctors_hospital`
--

CREATE TABLE IF NOT EXISTS `doctors_hospital` (
`id` int(10) unsigned NOT NULL,
  `doctor_id` int(10) unsigned NOT NULL,
  `hospital_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `doctors_hospital`
--

INSERT INTO `doctors_hospital` (`id`, `doctor_id`, `hospital_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 1, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 1, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 1, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 1, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 1, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 1, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 1, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `hospital`
--

CREATE TABLE IF NOT EXISTS `hospital` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `street` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `zip` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone_mobile` int(11) NOT NULL,
  `phone_home` int(11) NOT NULL,
  `phone_fax` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `latitude` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `longitude` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `hospital`
--

INSERT INTO `hospital` (`id`, `name`, `city`, `street`, `zip`, `country`, `phone_mobile`, `phone_home`, `phone_fax`, `email`, `latitude`, `longitude`, `is_active`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Lanka Hospital', 'Narahenpita', 'colombo 04', '123', 'Sri Lanka', 221223454, 231304992, 2139804809, 'lanka@lankahospotal.lk', '7.196914', '79.891205', 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Asiri  Hospital', 'Borella', 'colombo 06', '4566', 'Sri Lanka', 2147483647, 2147483647, 2147483647, 'info@asiri hospital.lk', '7.196914', '79.891205', 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Ceymed Healthcare Services (Pvt) Ltd', 'Colombo 04', 'Kirulapana', '0003', 'Sri Lanka', 237483775, 2147483647, 2147483647, 'info@ceymedhealthcare.lk', '7.196914', '79.891205', 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Winlanka Hospital', 'Baththaramulla', 'colombo 10', '0004', 'Sri Lanka', 2147483647, 1234567897, 1234567865, 'info@winlankahospital.com', '7.196914', '79.891205', 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Metro Medi Care', 'Narahenpita', 'colombo 04', '13323', 'Sri Lanka', 221223454, 231304992, 2139804809, 'lanka@metromedicare.lk', '7.196914', '79.891205', 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'PHILIP  Hospital', 'Kalutara', 'Kalutara', '32534', 'Sri Lanka', 2147483647, 2147483647, 2147483647, 'info@philip.net', '7.196914', '79.891205', 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'Child Medicare Services (Pvt) Ltd', 'Colombo 02', 'Jaffna', '00012', 'Sri Lanka', 45645654, 456545456, 56456456, 'info@childmedicareservices.lk', '7.196914', '79.891205', 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'Elder Medicare Hospital', 'Thummulla junction', 'colombo 10', '00016', 'Sri Lanka', 34543534, 8798654, 1234567865, 'info@eldermedicareservices.com', '7.196914', '79.891205', 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2015_12_03_045100_create_patient_table', 1),
('2015_12_07_075411_create_doctor_table', 2),
('2015_12_07_092234_create_hospital_table', 3),
('2015_12_09_092341_create_doctors_hospital_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `patient`
--

CREATE TABLE IF NOT EXISTS `patient` (
`id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nic` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_of_birth` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone_mobile` int(11) NOT NULL,
  `phone_home` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `pin_delivery_method` enum('mail','sms') COLLATE utf8_unicode_ci NOT NULL,
  `is_active` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `patient`
--

INSERT INTO `patient` (`id`, `title`, `first_name`, `last_name`, `nic`, `address`, `country`, `date_of_birth`, `phone_mobile`, `phone_home`, `email`, `password`, `pin_delivery_method`, `is_active`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Mr', '21', '123', '232234', '12312321', '122112', '12/22/2015', 2147483647, 2147483647, '1232', '12231231', 'mail', 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Mr', 'sdfsdf', '123', '214234', '42344', '324234', '12/29/2015', 2147483647, 1241241241, '23421312', '324234', 'sms', 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'mrs', 'suwrna', 'gunathilaka', '997765234', 'colombo 10', 'Sri Lanka', '12/21/2015', 0, 0, 'swrana@123.lk', '123345', '', 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'mr', 'ewrwerwer', 'werwer', '343', '5345', '34534534', '12/28/2015', 111111111, 777777777, '534', 'eadb30aeb589bd162991fe965710ed52', '', 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'mr', 'sdfsdf', 'sdfsdf', '214234', '12312321', '324234', '12/08/2015', 2147483647, 2147483647, 'sw7777rana@123.lk', '5d7009220a974e94404889274d3a9553', 'sms', 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `doctor`
--
ALTER TABLE `doctor`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `doctor_email_unique` (`email`);

--
-- Indexes for table `doctors_hospital`
--
ALTER TABLE `doctors_hospital`
 ADD PRIMARY KEY (`id`), ADD KEY `doctors_hospital_hospital_id_foreign` (`hospital_id`), ADD KEY `doctors_hospital_doctor_id_foreign` (`doctor_id`);

--
-- Indexes for table `hospital`
--
ALTER TABLE `hospital`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `hospital_email_unique` (`email`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
 ADD KEY `password_resets_email_index` (`email`), ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `patient`
--
ALTER TABLE `patient`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `patient_email_unique` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `doctor`
--
ALTER TABLE `doctor`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `doctors_hospital`
--
ALTER TABLE `doctors_hospital`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `hospital`
--
ALTER TABLE `hospital`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `patient`
--
ALTER TABLE `patient`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `doctors_hospital`
--
ALTER TABLE `doctors_hospital`
ADD CONSTRAINT `doctors_hospital_doctor_id_foreign` FOREIGN KEY (`doctor_id`) REFERENCES `doctor` (`id`),
ADD CONSTRAINT `doctors_hospital_hospital_id_foreign` FOREIGN KEY (`hospital_id`) REFERENCES `hospital` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
